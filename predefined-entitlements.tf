locals {

  # Ensure that no grant changes can happen, even though we're granting admin access
  # See https://cloud.google.com/iam/docs/setting-limits-on-granting-roles for
  # details of this technique
  no_role_modifications = "api.getAttribute('iam.googleapis.com/modifiedGrantsByRole', []) == []"

  predefined_role_definitions = {
    org_read = {
      resource_target   = "organization"
      requires_approval = false

      role_bindings = [
        {
          # Admin Read access to security center
          # https://cloud.google.com/iam/docs/understanding-roles?authuser=1#securitycenter.adminViewer
          role = "roles/securitycenter.adminViewer"
          }, {
          # Provides permissions to list all resources and allow policies on them.
          # https://cloud.google.com/iam/docs/understanding-roles?authuser=1#iam.securityReviewer
          role = "roles/iam.securityReviewer"
          }, {
          # Deny Reviewer role, with permissions to read deny policies
          # https://cloud.google.com/iam/docs/understanding-roles?authuser=1#iam.denyReviewer
          role = "roles/iam.denyReviewer"
        }
      ]
    }

    project_read = {
      resource_target   = "project"
      requires_approval = false

      role_bindings = [
        {
          role = "roles/compute.networkViewer"
          }, {
          role = "roles/compute.viewer"
          }, {
          role = "roles/cloudsql.viewer"
          }, {
          role = "roles/container.clusterViewer"
          }, {
          role = "roles/container.viewer"
          }, {
          role = "roles/secretmanager.viewer"
          }, {
          role = "roles/redis.viewer"
          }, {
          role = "roles/logging.viewer"
        }
      ]
    }

    project_admin = {
      resource_target = "project"

      # we do not have a process in place for human approval
      requires_approval = false

      role_bindings = [

        # IMPORTANT: these roles could allow privilege escalated users to temporarily grant themselves admin access to sensitive resources,
        # which could lead to unauthorized access or data breaches.
        # To mitigate this risk, we've added a condition to the role bindings that prevents any role modifications have been made.
        # This ensures that the admin access can only be granted if no other role modifications have occurred, preventing unintended escalation of privileges.
        # ALL DANGEROUS ROLES SHOULD USE THE CONDITION TO PREVENT PRIVILEGE ESCALATION

        {
          role      = "roles/compute.networkAdmin"
          condition = local.no_role_modifications # NB: prevent privilege escalation
          }, {
          role      = "roles/compute.admin"
          condition = local.no_role_modifications # NB: prevent privilege escalation
          }, {
          role      = "roles/cloudsql.admin"
          condition = local.no_role_modifications # NB: prevent privilege escalation
          }, {
          role      = "roles/container.clusterAdmin"
          condition = local.no_role_modifications # NB: prevent privilege escalation
          }, {
          role      = "roles/container.admin"
          condition = local.no_role_modifications # NB: prevent privilege escalation
          }, {
          role      = "roles/secretmanager.admin"
          condition = local.no_role_modifications # NB: prevent privilege escalation
          }, {
          role      = "roles/redis.admin"
          condition = local.no_role_modifications # NB: prevent privilege escalation
          }, {
          role      = "roles/iap.tunnelResourceAccessor"
          condition = local.no_role_modifications # NB: prevent privilege escalation
          }, {
          # Why are we including roles/resourcemanager.projectIamAdmin here?
          # Reading: https://cloud.google.com/resource-manager/docs/access-control-proj
          # resourcemanager does not provide it's own viewer predefined role, instead
          # relying on the roles/browser basic role for view access.
          #
          # Unfortunately, this basic role cannot be used with PAM (only predefined or custom roles)
          # `roles/resourcemanager.projectIamAdmin` provides the required access, and with the condition,
          # is made safe, since the requester is unable to change permissions due to the condition.
          # Thus, roles/resourcemanager.projectIamAdmin is the best fit.
          role      = "roles/resourcemanager.projectIamAdmin"
          condition = local.no_role_modifications # NB: prevent privilege escalation
          }, {
          role      = "roles/logging.viewer"
          condition = local.no_role_modifications # NB: prevent privilege escalation
          }, {
          # This role is provides access to all Storage buckets used by the application of infrastructure
          role      = "roles/storage.admin"
          condition = local.no_role_modifications # NB: prevent privilege escalation
        }
      ]
    }
  }
}
