#!/bin/sh

# Note: uses sh instead of bash as there no bash in the terraform-docs container.

set -o errexit
set -o nounset

root="$(dirname "${0}")/.."
cd "${root}"

for i in $(find . -type f -name '*.tf' -not -path "./get/*" -exec dirname {} \; | sort -u); do
  (
    cd "${i}"
    printf '%s/' "$i"
    terraform-docs markdown table --lockfile=false --output-file README.md --hide-empty --hide requirements,providers .
  )
done
