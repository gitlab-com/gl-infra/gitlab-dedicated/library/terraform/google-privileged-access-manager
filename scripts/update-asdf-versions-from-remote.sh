#!/usr/bin/env bash

set -uo pipefail

script_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

cd "${script_dir}/.."

IFS='' read -r -d '' awk_script <<'EOF'
!/^ *#/ {
  system("sed " sed_option " -e 's@^" $1 " [0-9].*@" $1 " " $2 " # updated from " repository "@' .tool-versions")
}
EOF

set -e

update_tool_versions_from() {
  local repository=$1
  local ref=$2

  # GNU tools and BSD tools for userspace are often
  # incompatible.  Make sure that invoking sed to
  # change file in-place works on both Linux and
  # macOS (Darwin).
  sed_option="-i"
  [[ $OSTYPE == "darwin"* ]] && sed_option="-i \\\'\\\'"

  # Iterate over the tool-versions file and
  get_tool_versions_file "$repository" "$ref" | awk \
    -v "repository=${repository}" \
    -v "sed_option=${sed_option}" \
    "${awk_script}"

  # Update .gitlab-ci-asdf-versions.yml to match...
  ./scripts/update-asdf-version-variables.sh
}

get_tool_versions_file() {
  local repository=$1
  local ref=$2

  if [[ -n ${RENOVATE_GITLAB_TOKEN-} ]]; then
    curl \
      --location \
      --fail \
      --silent \
      --header "PRIVATE-TOKEN: ${RENOVATE_GITLAB_TOKEN}" \
      "https://gitlab.com/api/v4/projects/${repository//\//%2f}/repository/files/.tool-versions/raw?ref=${ref}"
    return
  fi

  # No renovate token, perform a local clone instead
  temp_dir="$(mktemp -d)"
  git clone "git@gitlab.com:${repository}.git" "${temp_dir}"
  trap 'rm -rf "${temp_dir}"' EXIT

  git -c advice.detachedHead=false -C "${temp_dir}" checkout "$ref"

  cat "${temp_dir}/.tool-versions"
}

# Use common .tool-versions versions from gitlab-com/gl-infra/gitlab-dedicated/dedicated-container-image
GL_DEDICATED_CONTAINER_IMAGE_VERSION_PREFIXED="$(yq .variables.GL_DEDICATED_CONTAINER_IMAGE_VERSION_PREFIXED .gitlab-ci-other-versions.yml)"
update_tool_versions_from gitlab-com/gl-infra/gitlab-dedicated/dedicated-container-image "${GL_DEDICATED_CONTAINER_IMAGE_VERSION_PREFIXED}"
