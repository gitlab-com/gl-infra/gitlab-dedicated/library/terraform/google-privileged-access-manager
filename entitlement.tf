# https://cloud.google.com/iam/docs/pam-create-entitlements#create_entitlements_programmatically
# Readonly access does not require an approval, only justification

locals {
  # org_id is always set, but if either project_id or folder_id are set
  # then we need to use specific values for each
  # this map provides these values with lookup logic below
  gcp_resources = {
    organization = {
      parent        = "organizations/${var.org_id}"
      resource_type = "cloudresourcemanager.googleapis.com/Organization"
      resource      = "//cloudresourcemanager.googleapis.com/organizations/${var.org_id}"
    }
    folder = {
      parent        = "folders/${coalesce(var.folder_id, "empty_value")}"
      resource_type = "cloudresourcemanager.googleapis.com/Folder"
      resource      = "//cloudresourcemanager.googleapis.com/folders/${coalesce(var.folder_id, "placeholder")}" # as this can be null, coalesce is used to gracefully fail
    }
    project = {
      parent        = "projects/${coalesce(var.project_id, "empty_value")}"
      resource_type = "cloudresourcemanager.googleapis.com/Project"
      resource      = "//cloudresourcemanager.googleapis.com/projects/${coalesce(var.project_id, "placeholder")}" # as this can be null, coalesce is used to gracefully fail
    }

  }
  # if folder or project are supplied then set a boolean to indicate we should use them
  use_folder  = var.folder_id != null
  use_project = var.folder_id == null && var.project_id != null

  resource_target = local.use_folder ? "folder" : local.use_project ? "project" : "organization"
  resource_params = local.gcp_resources[local.resource_target]

  restricted_role_bindings = [for rb in var.allowed_dangerous_role_bindings : { role : rb, condition : local.no_role_modifications }]
}


resource "google_privileged_access_manager_entitlement" "item" {
  provider = google-beta

  entitlement_id       = var.entitlement_id
  location             = "global"
  max_request_duration = "7200s"
  parent               = local.resource_params.parent

  eligible_users {
    principals = var.eligible_members
  }

  privileged_access {
    gcp_iam_access {
      resource_type = local.resource_params.resource_type
      resource      = local.resource_params.resource

      # "Custom" role_bindings
      dynamic "role_bindings" {
        for_each = var.role_bindings
        content {
          role                 = role_bindings.value.role
          condition_expression = role_bindings.value.condition
        }
      }

      # Predefined role_bindings
      dynamic "role_bindings" {
        for_each = var.predefined_entitlement_role != null ? local.predefined_role_definitions[var.predefined_entitlement_role].role_bindings : []
        content {
          role                 = role_bindings.value.role
          condition_expression = lookup(role_bindings.value, "condition", null)
        }
      }

      # Restricted dangerous role bindings
      dynamic "role_bindings" {
        for_each = local.restricted_role_bindings
        content {
          role                 = role_bindings.value.role
          condition_expression = role_bindings.value.condition
        }
      }
    }
  }

  requester_justification_config {
    #  The requester has to provide a justification in the form of free flowing text.
    unstructured {}
  }

  additional_notification_targets {
    admin_email_recipients     = []
    requester_email_recipients = []
  }

  dynamic "approval_workflow" {
    for_each = length(var.approver_members) == 0 ? [] : [1]
    content {
      manual_approvals {
        require_approver_justification = false

        steps {
          approvals_needed          = 1
          approver_email_recipients = []

          approvers {
            principals = var.approver_members
          }
        }
      }
    }
  }

  lifecycle {
    precondition {
      condition = (
        var.predefined_entitlement_role != null ?
        local.predefined_role_definitions[var.predefined_entitlement_role].resource_target == local.resource_target :
        true
      )
      error_message = "The predefined entitlement role target is incorrect"
    }

    precondition {
      condition = (
        var.predefined_entitlement_role != null ?
        !local.predefined_role_definitions[var.predefined_entitlement_role].requires_approval || length(var.approver_members) > 0 :
        true
      )
      error_message = "The predefined_entitlements selected required approvers to be configured"
    }

    precondition {
      condition = (
        var.predefined_entitlement_role == null ?
        length(var.role_bindings) > 0 :
        true
      )
      error_message = "role_bindings must be configured if predefined_entitlements is not used"
    }
  }

  depends_on = [time_sleep.wait_30_seconds]
}
