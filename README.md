# google-privileged-access-manager

This module provisions Google Cloud's Privileged Access Manager using GitLab breakglass policies and defaults.

## Using this module

### Non-approved Project Usage with Predefined Project Read Role

Example of using the predefined entitlement role `project_read`, which will grant the requester read-only access to a project.

1. `predefined_entitlement_role` is set to `project_read`
2. `approver_members` is not set, therefore no approvals required

```terraform
module "project_readonly_entitlement" {
  source = "gitlab.com/gitlab-com/google-privileged-access-manager/local"
  version = "1.0.0"

  org_id                      = "1234567890"
  project_id                  = var.gcp_project_id
  entitlement_id              = "entititlement-readonly"
  predefined_entitlement_role = "project_read"
  eligible_members            = [
      "group:security-group@gitlab.com",
  ]
}
```


### Approved Project Usage with Predefined Project Admin Role

Example of using the predefined entitlement role `project_admin`, which will grant the requester admin access to a project.

1. `predefined_entitlement_role` is set to `project_admin`
2. For `project_admin` predefined entitlement, `approver_members` is mandatory, and configured to a manager group.

```terraform
module "project_readonly_entitlement" {
  source = "gitlab.com/gitlab-com/google-privileged-access-manager/local"
  version = "1.0.0"

  org_id                      = "1234567890"
  project_id                  = var.gcp_project_id
  entitlement_id              = "entititlement-admin"
  predefined_entitlement_role = "project_admin"
  eligible_members            = [
      "group:security-group@gitlab.com",
  ]
  approver_members            = [
      "group:security-managers@gitlab.com",
  ]
}
```

### Non-approved Project Usage with Custom Roles

Example usage for project roles, no approval required.

```terraform
module "readonly_entitlement" {
  source  = "gitlab.com/gitlab-com/google-privileged-access-manager/local"
  version = "1.0.0"

  org_id           = "1234567890"
  project_id       = var.gcp_project_id
  entitlement_id   = "entitlement-readonly"
  eligible_members = [
    "group:security-group@gitlab.com",
  ]

  role_bindings = [{
      role = "roles/compute.networkViewer"
    }, {
      role = "roles/compute.viewer"
    }]
}
```

### Non-approved Organization Usage with Custom Roles

Example usage for project roles, no approval required.

```terraform
module "readonly_entitlement" {
  source  = "gitlab.com/gitlab-com/google-privileged-access-manager/local"
  version = "1.0.0"

  org_id           = "1234567890"
  entitlement_id   = "entitlement-readonly"
  eligible_members = [
    "group:security-group@gitlab.com",
  ]

  role_bindings = [{
      role = "roles/securitycenter.adminViewer"
    }]
}
```

## Important Security Warning

**IMPORTANT**: read this warning before adding roles to this entitlement:
Be careful when including the following types of roles in an entitlement:

* Roles with permissions to grant and revoke IAM roles (that is, roles with permission names that end in `setIamPolicy`).
* Roles with the `iam.roles.update` permission, which lets users modify custom roles.

These types of roles contain permissions that could let a user modify their own IAM permissions.
As a result, requesting principals can use these roles to increase their own access to resources,
or give themselves additional access to resources.

For example, imagine a user that has a custom role with very limited permissions.
If this user successfully requests a grant against an entitlement with the Role Administrator role (`roles/iam.roleAdmin`),
then they can use the permissions in that role to add the `resourcemanager.projects.setIamPolicy` permission to their custom role.
This permission would let them grant and revoke all IAM roles for the project, even after the grant expires.

See <https://cloud.google.com/iam/docs/pam-create-entitlements#create_entitlements_programmatically>

### Validation

This module will attempt to prevent incorrect usage, based on a set of known risky roles. However, new roles may be introduced,
and custom roles will circumvent the checks. Authors should review all permissions before adding a role.

## Module Inputs and Output

<!-- BEGIN_TF_DOCS -->


## Resources

| Name | Type |
|------|------|
| [google-beta_google_privileged_access_manager_entitlement.item](https://registry.terraform.io/providers/hashicorp/google-beta/latest/docs/resources/google_privileged_access_manager_entitlement) | resource |
| [google_project_iam_member.service_agent](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/project_iam_member) | resource |
| [google_project_service.pam](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/project_service) | resource |
| [time_sleep.wait_30_seconds](https://registry.terraform.io/providers/hashicorp/time/latest/docs/resources/sleep) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_allowed_dangerous_role_bindings"></a> [allowed\_dangerous\_role\_bindings](#input\_allowed\_dangerous\_role\_bindings) | Potentially dangerous role bindings to be created on successful grant. These role bindings will be internally set with a condition to prevent role modifications. | `list(string)` | `[]` | no |
| <a name="input_approver_members"></a> [approver\_members](#input\_approver\_members) | Members who can approve breakglass using Privileged Access Manager | `set(string)` | `[]` | no |
| <a name="input_eligible_members"></a> [eligible\_members](#input\_eligible\_members) | Members who can request breakglass using Privileged Access Manager | `set(string)` | n/a | yes |
| <a name="input_entitlement_id"></a> [entitlement\_id](#input\_entitlement\_id) | The ID to use for this Entitlement. This will become the last part of the resource name. This value should be unique among all other Entitlements under the specified parent. | `string` | n/a | yes |
| <a name="input_folder_id"></a> [folder\_id](#input\_folder\_id) | ID for the Organization Folder this is being applied to. When null, or unset this will be ignored. | `string` | `null` | no |
| <a name="input_org_id"></a> [org\_id](#input\_org\_id) | Organization ID for Google Organization this is being applied to. | `string` | n/a | yes |
| <a name="input_predefined_entitlement_role"></a> [predefined\_entitlement\_role](#input\_predefined\_entitlement\_role) | Chose a predefined set of entitlement role\_bindings for this entitlement. Can be used in place of role\_bindings, or in addition to them. | `string` | `null` | no |
| <a name="input_project_id"></a> [project\_id](#input\_project\_id) | Project ID for Google Project this is being applied to. When null, PAM will be applied to the project. | `string` | `null` | no |
| <a name="input_role_bindings"></a> [role\_bindings](#input\_role\_bindings) | Role bindings to be created on successful grant. | <pre>list(object({<br/>    role      = string<br/>    condition = optional(string)<br/>  }))</pre> | `[]` | no |
<!-- END_TF_DOCS -->


## Hacking on google-privileged-access-manager

### Preparing your Environment

1. Follow the developer setup guide at <https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks/-/blob/main/docs/developer-setup.md>.
1. Clone this project
1. Run `scripts/prepare-dev-env.sh`
