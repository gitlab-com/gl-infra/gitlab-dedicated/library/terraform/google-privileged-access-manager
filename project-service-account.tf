resource "google_project_service" "pam" {
  count = var.project_id != null ? 1 : 0

  project = var.project_id
  service = "privilegedaccessmanager.googleapis.com"

  disable_dependent_services = false
  disable_on_destroy         = false
}

# Grant the PAM serviceAgent access to the organization PAM service account
resource "google_project_iam_member" "service_agent" {
  count = var.project_id != null ? 1 : 0

  project = var.project_id
  role    = "roles/privilegedaccessmanager.serviceAgent"
  member  = "serviceAccount:service-org-${var.org_id}@gcp-sa-pam.iam.gserviceaccount.com"

  depends_on = [
    google_project_service.pam
  ]
}

# This is really unbelievable, but a sleep here is an official recommendation from Google.
# google_project_service returns before the service is really activated, and this
# timing is worse for newer projects.
#
# https://registry.terraform.io/providers/hashicorp/google/latest/docs/guides/google_project_service
resource "time_sleep" "wait_30_seconds" {
  count = var.project_id != null ? 1 : 0

  create_duration = "30s"

  depends_on = [google_project_service.pam, google_project_iam_member.service_agent]
}
