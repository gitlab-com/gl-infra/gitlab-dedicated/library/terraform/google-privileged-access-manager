terraform {
  required_version = "~> 1.6"

  required_providers {
    google = {
      source  = "hashicorp/google"
      version = ">= 5.31.1"
    }

    google-beta = {
      source  = "hashicorp/google-beta"
      version = ">= 5.31.1"
    }

    time = {
      source  = "hashicorp/time"
      version = ">= 0.11.1"
    }
  }
}
