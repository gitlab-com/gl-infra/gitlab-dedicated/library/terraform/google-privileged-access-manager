variable "org_id" {
  type        = string
  description = "Organization ID for Google Organization this is being applied to."
  nullable    = false
}

variable "project_id" {
  type        = string
  description = "Project ID for Google Project this is being applied to. When null, PAM will be applied to the project."
  nullable    = true
  default     = null
}

variable "folder_id" {
  type        = string
  description = "ID for the Organization Folder this is being applied to. When null, or unset this will be ignored."
  nullable    = true
  default     = null
}

variable "eligible_members" {
  description = "Members who can request breakglass using Privileged Access Manager"
  type        = set(string)
  nullable    = false
}


variable "approver_members" {
  description = "Members who can approve breakglass using Privileged Access Manager"
  type        = set(string)
  nullable    = true
  default     = []
}

variable "predefined_entitlement_role" {
  description = "Chose a predefined set of entitlement role_bindings for this entitlement. Can be used in place of role_bindings, or in addition to them."
  type        = string
  nullable    = true
  default     = null

  validation {
    condition = contains([
      null,
      "org_read",
      "project_read",
      "project_admin",
    ], coalesce(var.predefined_entitlement_role, "org_read"))
    error_message = "predefined_entitlements must be one of: org_read, project_read, project_admin"
  }
}

variable "entitlement_id" {
  type        = string
  description = "The ID to use for this Entitlement. This will become the last part of the resource name. This value should be unique among all other Entitlements under the specified parent."
  nullable    = false
}

variable "role_bindings" {
  type = list(object({
    role      = string
    condition = optional(string)
  }))
  description = "Role bindings to be created on successful grant."
  nullable    = true
  default     = []

  ## IMPORTANT: read this warning before adding roles to this entitlement:
  ## Be careful when including the following types of roles in an entitlement:
  ##
  ## * Roles with permissions to grant and revoke IAM roles (that is, roles with permission names that end in setIamPolicy).
  ## * Roles with the iam.roles.update permission, which lets users modify custom roles.
  ##
  ## These types of roles contain permissions that could let a user modify their own IAM permissions.
  ## As a result, requesting principals can use these roles to increase their own access to resources,
  ## or give themselves additional access to resources.
  ##
  ## For example, imagine a user that has a custom role with very limited permissions.
  ## If this user successfully requests a grant against an entitlement with the Role Administrator role (roles/iam.roleAdmin),
  ## then they can use the permissions in that role to add the resourcemanager.projects.setIamPolicy permission to their custom role.
  ## This permission would let them grant and revoke all IAM roles for the project, even after the grant expires.
  ##
  ## See https://cloud.google.com/iam/docs/pam-create-entitlements#create_entitlements_programmatically

  validation {
    condition = alltrue([
      for binding in var.role_bindings : !contains([
        # Roles with permissions to grant and revoke IAM roles (that is, roles with permission names that end in setIamPolicy).
        # Obtained with this command:
        # curl https://raw.githubusercontent.com/iann0036/iam-dataset/main/gcp/permissions.json | \
        # jq -r '. as $b|[.|keys|.[]|select(contains("setIamPolicy"))|$b[.]]|flatten|[.[]|.id]|unique|sort|.[]'
        "roles/accesscontextmanager.policyAdmin",
        "roles/aiplatform.admin",
        "roles/aiplatform.colabEnterpriseAdmin",
        "roles/aiplatform.entityTypeOwner",
        "roles/aiplatform.extensionCustomCodeServiceAgent",
        "roles/aiplatform.featurestoreAdmin",
        "roles/aiplatform.featurestoreUser",
        "roles/aiplatform.notebookRuntimeAdmin",
        "roles/analyticshub.admin",
        "roles/analyticshub.listingAdmin",
        "roles/apigateway.admin",
        "roles/apigee.admin",
        "roles/apigee.environmentAdmin",
        "roles/apigeeregistry.admin",
        "roles/appengineflex.serviceAgent",
        "roles/apphub.admin",
        "roles/artifactregistry.admin",
        "roles/automl.admin",
        "roles/autoscaling.sitesAdmin",
        "roles/backupdr.admin",
        "roles/beyondcorp.admin",
        "roles/beyondcorp.clientConnectorAdmin",
        "roles/bigquery.admin",
        "roles/bigquery.connectionAdmin",
        "roles/bigquery.dataOwner",
        "roles/bigquery.studioAdmin",
        "roles/bigtable.admin",
        "roles/billing.admin",
        "roles/binaryauthorization.attestorsAdmin",
        "roles/binaryauthorization.policyAdmin",
        "roles/certificatemanager.owner",
        "roles/chronicle.serviceAgent",
        "roles/cloudbuild.connectionAdmin",
        "roles/clouddeploy.admin",
        "roles/clouddeploy.customTargetTypeAdmin",
        "roles/cloudfunctions.admin",
        "roles/cloudfunctions.serviceAgent",
        "roles/cloudkms.admin",
        "roles/cloudprivatecatalogproducer.admin",
        "roles/cloudprivatecatalogproducer.orgAdmin",
        "roles/cloudsupport.admin",
        "roles/cloudtasks.admin",
        "roles/cloudtasks.queueAdmin",
        "roles/cloudtpu.serviceAgent",
        "roles/composer.ServiceAgentV2Ext",
        "roles/composer.environmentAndStorageObjectAdmin",
        "roles/composer.serviceAgent",
        "roles/composer.worker",
        "roles/compute.admin",
        "roles/compute.instanceAdmin",
        "roles/compute.instanceAdmin.v1",
        "roles/compute.loadBalancerAdmin",
        "roles/compute.networkAdmin",
        "roles/compute.orgFirewallPolicyAdmin",
        "roles/compute.orgSecurityPolicyAdmin",
        "roles/compute.orgSecurityPolicyUser",
        "roles/compute.orgSecurityResourceAdmin",
        "roles/compute.securityAdmin",
        "roles/compute.storageAdmin",
        "roles/compute.xpnAdmin",
        "roles/config.admin",
        "roles/connectors.admin",
        "roles/connectors.customConnectorAdmin",
        "roles/connectors.endpointAttachmentAdmin",
        "roles/connectors.managedZoneAdmin",
        "roles/container.serviceAgent",
        "roles/containeranalysis.admin",
        "roles/contentwarehouse.admin",
        "roles/contentwarehouse.documentAdmin",
        "roles/datacatalog.admin",
        "roles/datacatalog.categoryAdmin",
        "roles/datacatalog.entryGroupOwner",
        "roles/datacatalog.entryOwner",
        "roles/datacatalog.glossaryOwner",
        "roles/datacatalog.tagTemplateOwner",
        "roles/dataconnectors.connectorAdmin",
        "roles/dataflow.serviceAgent",
        "roles/dataform.admin",
        "roles/dataform.codeOwner",
        "roles/datafusion.admin",
        "roles/datafusion.serviceAgent",
        "roles/datalabeling.serviceAgent",
        "roles/datamigration.admin",
        "roles/datapipelines.serviceAgent",
        "roles/dataplex.admin",
        "roles/dataplex.aspectTypeOwner",
        "roles/dataplex.bindingAdmin",
        "roles/dataplex.catalogAdmin",
        "roles/dataplex.dataScanAdmin",
        "roles/dataplex.developer",
        "roles/dataplex.entryGroupOwner",
        "roles/dataplex.entryTypeOwner",
        "roles/dataplex.serviceAgent",
        "roles/dataplex.taxonomyAdmin",
        "roles/dataprep.serviceAgent",
        "roles/dataproc.admin",
        "roles/dataproc.serviceAgent",
        "roles/dataproc.worker",
        "roles/datastream.admin",
        "roles/datastudio.admin",
        "roles/datastudio.manager",
        "roles/discoveryengine.serviceAgent",
        "roles/dlp.serviceAgent",
        "roles/domains.admin",
        "roles/earthengine.admin",
        "roles/earthengine.appsPublisher",
        "roles/edgecontainer.admin",
        "roles/edgenetwork.admin",
        "roles/eventarc.admin",
        "roles/firebase.admin",
        "roles/firebase.developAdmin",
        "roles/firebase.managementServiceAgent",
        "roles/firebase.sdkAdminServiceAgent",
        "roles/firebasemods.serviceAgent",
        "roles/genomics.admin",
        "roles/genomics.serviceAgent",
        "roles/gkebackup.admin",
        "roles/gkebackup.backupAdmin",
        "roles/gkebackup.restoreAdmin",
        "roles/gkehub.admin",
        "roles/gkehub.crossProjectServiceAgent",
        "roles/gkehub.scopeAdmin",
        "roles/gkemulticloud.serviceAgent",
        "roles/gkeonprem.admin",
        "roles/healthcare.annotationStoreAdmin",
        "roles/healthcare.consentStoreAdmin",
        "roles/healthcare.datasetAdmin",
        "roles/healthcare.dicomStoreAdmin",
        "roles/healthcare.fhirStoreAdmin",
        "roles/healthcare.hl7V2StoreAdmin",
        "roles/iam.securityAdmin",
        "roles/iam.serviceAccountAdmin",
        "roles/iam.workforcePoolAdmin",
        "roles/iap.admin",
        "roles/identityplatform.admin",
        "roles/identitytoolkit.admin",
        "roles/ids.admin",
        "roles/krmapihosting.admin",
        "roles/krmapihosting.anthosApiEndpointServiceAgent",
        "roles/kuberun.eventsControlPlaneServiceAgent",
        "roles/lifesciences.serviceAgent",
        "roles/logging.admin",
        "roles/managedidentities.admin",
        "roles/managedidentities.backupAdmin",
        "roles/managedidentities.domainAdmin",
        "roles/managedidentities.peeringAdmin",
        "roles/metastore.admin",
        "roles/metastore.metadataOwner",
        "roles/metastore.serviceAgent",
        "roles/ml.admin",
        "roles/ml.developer",
        "roles/ml.jobOwner",
        "roles/ml.modelOwner",
        "roles/ml.serviceAgent",
        "roles/multiclusteringress.serviceAgent",
        "roles/multiclusterservicediscovery.serviceAgent",
        "roles/networkconnectivity.hubAdmin",
        "roles/networkconnectivity.serviceAgent",
        "roles/networkconnectivity.spokeAdmin",
        "roles/networkmanagement.admin",
        "roles/notebooks.admin",
        "roles/notebooks.legacyAdmin",
        "roles/notebooks.serviceAgent",
        "roles/owner",
        "roles/privateca.admin",
        "roles/privilegedaccessmanager.admin",
        "roles/privilegedaccessmanager.folderServiceAgent",
        "roles/privilegedaccessmanager.organizationServiceAgent",
        "roles/privilegedaccessmanager.projectServiceAgent",
        "roles/privilegedaccessmanager.serviceAgent",
        "roles/pubsub.admin",
        "roles/resourcemanager.folderAdmin",
        "roles/resourcemanager.folderIamAdmin",
        "roles/resourcemanager.organizationAdmin",
        "roles/resourcemanager.projectIamAdmin",
        "roles/resourcemanager.tagAdmin",
        "roles/run.admin",
        "roles/runtimeconfig.admin",
        "roles/secretmanager.admin",
        "roles/securedlandingzone.bqdwProjectRemediator",
        "roles/securedlandingzone.serviceAgent",
        "roles/securesourcemanager.admin",
        "roles/securesourcemanager.instanceOwner",
        "roles/securesourcemanager.repoAdmin",
        "roles/securitycenter.admin",
        "roles/securitycenter.sourcesAdmin",
        "roles/servicebroker.admin",
        "roles/servicedirectory.admin",
        "roles/servicemanagement.admin",
        "roles/source.admin",
        "roles/spanner.admin",
        "roles/spanner.backupAdmin",
        "roles/spanner.databaseAdmin",
        "roles/storage.admin",
        "roles/storage.folderAdmin",
        "roles/storage.legacyBucketOwner",
        "roles/storage.legacyObjectOwner",
        "roles/storage.objectAdmin",
        "roles/visionai.admin",
        "roles/visualinspection.serviceAgent",
        "roles/vmwareengine.vmwareengineAdmin",
        "roles/workstations.admin"
      ], binding.role)
    ])
    error_message = "Rolebindings should not use roles with permissions to grant and revoke IAM roles (that is, roles with permission names that end in setIamPolicy). See https://cloud.google.com/iam/docs/pam-create-entitlements#create_entitlements_programmatically"
  }

  validation {
    condition = alltrue([
      for binding in var.role_bindings : !contains([
        # Roles with the iam.roles.update permission:
        # https://gcp.permissions.cloud/iam/iam#iam.roles.update
        # Obtained from https://github.com/search?q=repo:iann0036/iam-dataset+path:/%5Egcp%5C/roles/+iam.roles.update&type=code
        "roles/clouddeploymentmanager.serviceAgent",
        "roles/iam.organizationRoleAdmin",
        "roles/iam.roleAdmin",
      ], binding.role)
    ])
    error_message = "Rolebindings should not use roles with the `iam.roles.update` permission. See https://cloud.google.com/iam/docs/pam-create-entitlements#create_entitlements_programmatically"
  }
}

variable "allowed_dangerous_role_bindings" {
  type        = list(string)
  description = "Potentially dangerous role bindings to be created on successful grant. These role bindings will be internally set with a condition to prevent role modifications."
  nullable    = true
  default     = []

  validation {
    condition = alltrue([
      for binding in var.allowed_dangerous_role_bindings : !contains([
        # Roles with the iam.roles.update permission:
        # https://gcp.permissions.cloud/iam/iam#iam.roles.update
        # Obtained from https://github.com/search?q=repo:iann0036/iam-dataset+path:/%5Egcp%5C/roles/+iam.roles.update&type=code
        "roles/clouddeploymentmanager.serviceAgent",
        "roles/iam.organizationRoleAdmin",
        "roles/iam.roleAdmin",
      ], binding)
    ])
    error_message = "Rolebindings should not use roles with the `iam.roles.update` permission. See https://cloud.google.com/iam/docs/pam-create-entitlements#create_entitlements_programmatically"
  }
}
