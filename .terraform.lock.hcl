# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/google" {
  version     = "6.23.0"
  constraints = ">= 5.4.0"
  hashes = [
    "h1:AQqvilOF4FnbXO/c84HsNkJ+TyZEs+N12iHYFnwvNOM=",
    "h1:Gr39ABNw+A6lwP2gPG+yCzGmU5T97iI5qT0XLCd3Dh4=",
    "h1:H9bo7LCZafcq9OmqJomfxrcc6B3jKmIc3wpdHodfpYk=",
    "h1:NTdicRL7Atqk8fM3B0PQguImzHdZWQwOZwPW0AzkQm0=",
    "h1:TLMWskwzbDsyfi6LaqTD0ckSTT4rtpGLF/QCojaDiec=",
    "h1:biGOmqtXxJKQl2BRp23Ars11UpSmZenahVIsm4ppFfk=",
    "h1:mdB2KHl+8oFUWBWGnfbOaqX2y8H+Ptm0Fdub4feX99U=",
    "h1:oAaJhkB2s1y4fSESXi73zJxdHhC7NdB7rdFN6KH2uY8=",
    "h1:rENW0F5QJbBPmGBwoD+pzZw65D7W3LAfBZMFr9ncE80=",
    "h1:uBPMb39cwdZE6XAPkTynWdIdChkj1IPrhqPHd+BffE4=",
    "h1:wp55nTaA9gz0Imll8+4PYeOONzQYKKDWIcoB6qUoJaE=",
    "zh:032dd78eff887a673a1067008a8e47a69983bbcea9f41832320470247a76863a",
    "zh:1af89f75142cf9c54499a466c8dc7055e2bbf02771a6b8c8cd57eb13dce9a800",
    "zh:3696a8e72c6cef80fec3c3574fc8519f0410f23f6dc3e3540d2f03345c140d38",
    "zh:58a15c71ae128ff64117c1c6b9ccf8ab2ac3e8f9c2c52957d8327f93495f62b1",
    "zh:70ba2909611e8d1cc8009567e50e195c4269e6582d6a6fa0bce0d4e6313ab8d5",
    "zh:8f8489d1eb8c189d59dc85e519e51ab4c4b1940e4d72450ae130ba752028fa01",
    "zh:99c8c4e8dc67a7ab597d46ed566f64c4409761276f34bd863457a37254620fa6",
    "zh:9b24d53440e8d7e06020e7b3aeca0dde4f1a3ef997d7da05ff3acb918896fcc2",
    "zh:b3667fd6057997dbf0bd0179ddf686c272d4ab4fc7da6a03fdf2fad31ad4ecb5",
    "zh:cc6df6d2291a337a5f434b7335c693164c6987604b9690d6b953b796d8eaa08a",
    "zh:d871f39c3c5b63995793c9a70f107e52ca699d211c770a6e7ebc398ba59bcdc7",
    "zh:f569b65999264a9416862bca5cd2a6177d94ccb0424f3a4ef424428912b9cb3c",
  ]
}

provider "registry.terraform.io/hashicorp/google-beta" {
  version     = "6.23.0"
  constraints = ">= 5.4.0"
  hashes = [
    "h1:6ei7qcLoyceSdoj0bJNyTZso0TikV5QIDsThcJEfJOY=",
    "h1:7XB8f7ssjc48vV29mfxM0UgEFuF6Ol1SWB2XEtKVnMY=",
    "h1:PpMg/AxUoTMLANgYZ7WyBmSv/b1P+pQpMd+j5mbhPRo=",
    "h1:SH9rbcSJGvpgQf3pg2rTyIMv/AWGvLHcV8JrK2jMJWs=",
    "h1:a0VVLu5Kyzjw8rTIXYFBzBSL05Vvo/p2ji0reTQvIzY=",
    "h1:fy/FPlcVyla16+uQ3+LA6MWmZ1sIIiDlW/FgEa2d/fo=",
    "h1:i/TKWST1xe697zDOVDeHcXpjZ4TBxxJK5X7+DS4ozk4=",
    "h1:iF2UaqF5bu4FV7IZ5w4naDFl6b8FoWpOYFCk5MtjYOI=",
    "h1:iK6EtFNF1weQMa/AxMHldxNO+eFdcfM0RnJOhXdma2o=",
    "h1:jvW9OQ1N9FAAs73EjaZ+iXbpEmd1vyMTBiqynSw8iFU=",
    "h1:vCMUYsleCU0G+n6NAj83nJis5uFO4BnXwFtRoo2nwjM=",
    "zh:0801ceda0de3f61745d379aef0bfe6c3a918c597a097f91e64791fb77d44dd68",
    "zh:3b8b48de49892f25af14ce0e9abde055a3b008011dd9f7907706067348c32126",
    "zh:74a20663b00e0eb8bcb0305c3180e7061dd5f0b1d77f36768c3f58f7a773672f",
    "zh:763e2f0e69a03a4c85e9472b2c5af13e683519b92e3c4bad8fa4ecd650c2b256",
    "zh:99e9379b1511fd75917a5bab01be0fbec8ac45f4a2b811571ca8cec7326f5f6d",
    "zh:b28199d64772d7536704554b0fe66954e6c7cef1e15d4a488252c8da8a6b3648",
    "zh:ce467d73661458e88163a9e33e2142480bf01aaf3477a87a45bbb9227b65c2ea",
    "zh:d8fb5ee8d07bcaf717f09cebe27fe247a1c9a6a253476bba64a3048f094c377b",
    "zh:dbdb5dc2f18b1b48b501df2f3a4f201c8646b0c765d01b9fe69116e81d71ce53",
    "zh:e3e5f2a44734bc23c4fac7b8e4f2b368f9bbde68295129ec3290fa135d1de224",
    "zh:eba860faa6b49d917460eb8f3594bda0be8b8ca3f5f17465096148717d5f600e",
    "zh:f569b65999264a9416862bca5cd2a6177d94ccb0424f3a4ef424428912b9cb3c",
  ]
}

provider "registry.terraform.io/hashicorp/time" {
  version     = "0.12.1"
  constraints = ">= 0.11.1"
  hashes = [
    "h1:2M/A16wx+gTVyPWDSMLJL8TMgHjuolTjgkwCoKUPJ4k=",
    "h1:6BhxSYBJdBBKyuqatOGkuPKVenfx6UmLdiI13Pb3his=",
    "h1:ET12R5bbv8BLca4F9omDWWaHsReQANhillXmG902H/g=",
    "h1:JzYsPugN8Fb7C4NlfLoFu7BBPuRVT2/fCOdCaxshveI=",
    "h1:OI6VWK5ZU5Ukea45RDVGs6f9gXaclQHg/PyCt+fcPLI=",
    "h1:S0pBimZwNWWMwG+Y702fNCV9JjO8VD/itS4D4ozalKk=",
    "h1:VgFDnbNB6f13IXMkO9dRNNkcJFk0/SOM0e82qhO1e8I=",
    "h1:W+CZnsWJGAmWxU5o22w65VlOl9VBOnxeOg90tMo4Zcw=",
    "h1:gAk/pRJ/Bo6ilcKgZ650KslZuwu4X6QQKc94NlfBopE=",
    "h1:j+ED7j0ZFJ4EDx7sdna76wsiIf397toylDN0dFi6v0U=",
    "h1:ny87bLSd1q3AcQNBXmKhUHRBErwuPEX/nCa05C7tyF0=",
    "zh:090023137df8effe8804e81c65f636dadf8f9d35b79c3afff282d39367ba44b2",
    "zh:26f1e458358ba55f6558613f1427dcfa6ae2be5119b722d0b3adb27cd001efea",
    "zh:272ccc73a03384b72b964918c7afeb22c2e6be22460d92b150aaf28f29a7d511",
    "zh:438b8c74f5ed62fe921bd1078abe628a6675e44912933100ea4fa26863e340e9",
    "zh:78d5eefdd9e494defcb3c68d282b8f96630502cac21d1ea161f53cfe9bb483b3",
    "zh:85c8bd8eefc4afc33445de2ee7fbf33a7807bc34eb3734b8eefa4e98e4cddf38",
    "zh:98bbe309c9ff5b2352de6a047e0ec6c7e3764b4ed3dfd370839c4be2fbfff869",
    "zh:9c7bf8c56da1b124e0e2f3210a1915e778bab2be924481af684695b52672891e",
    "zh:d2200f7f6ab8ecb8373cda796b864ad4867f5c255cff9d3b032f666e4c78f625",
    "zh:d8c7926feaddfdc08d5ebb41b03445166df8c125417b28d64712dccd9feef136",
    "zh:e2412a192fc340c61b373d6c20c9d805d7d3dee6c720c34db23c2a8ff0abd71b",
    "zh:e6ac6bba391afe728a099df344dbd6481425b06d61697522017b8f7a59957d44",
  ]
}
